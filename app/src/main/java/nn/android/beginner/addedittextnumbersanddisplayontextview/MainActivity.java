package nn.android.beginner.addedittextnumbersanddisplayontextview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void add(View v) {
        // initialize objects
        TextView result = (TextView)findViewById(R.id.result);
        EditText et1 = (EditText)findViewById(R.id.editText1);
        EditText et2 = (EditText)findViewById(R.id.editText2);

        // get text from edit text boxes and convert into double
        double a = Double.parseDouble(String.valueOf(et1.getText()));
        double b = Double.parseDouble(String.valueOf(et2.getText()));

        // add them
        double c = a + b;

        // display addition on TextView
        result.setText("Added : " + c);
    }
}

